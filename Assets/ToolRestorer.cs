﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ToolRestorer : MonoBehaviour
{
    public GameObject[] tools;
    private Interactable interactableObject;

    private Vector3[] positons;
    private Quaternion[] rotations;

    private Vector3 pos;
    private Quaternion rot;

    void Start()
    {
        interactableObject = GetComponent<Interactable>();
        positons = new Vector3[tools.Length];
        rotations = new Quaternion[tools.Length];
        for (var i = 0; i < tools.Length; i++)
        {
            positons[i] = tools[i].transform.position;
            rotations[i] = tools[i].transform.rotation;
        }
        pos = transform.position;
        rot = transform.rotation;
    }

    void FixedUpdate()
    {
        if (interactableObject.attachedToHand != null)
        {
            for (var i = 0; i < tools.Length; i++)
            {
                tools[i].transform.position = positons[i];
                tools[i].transform.rotation = rotations[i];
            }
            interactableObject.attachedToHand.DetachObject(gameObject);
        }
        transform.position = pos;
        transform.rotation = rot;
    }
}
