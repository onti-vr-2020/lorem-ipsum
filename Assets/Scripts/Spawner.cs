﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject spawnObject;
    [SerializeField] private Transform spawnPoint;

    private GameObject currentObject;
    private bool isFixed;
    public float delay;

    private void Start()
    {
        SpawnObject();
    }

    public void SpawnObject()
    {
        isFixed = false;
        StopCoroutine(SpawnContiniuously());
        StartCoroutine(SpawnContiniuously());
        currentObject = Instantiate(spawnObject, spawnPoint.position, spawnObject.transform.rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == currentObject.gameObject)
        {
            isFixed = true;
            currentObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == currentObject.gameObject)
        {
            SpawnObject();
        }
    }

    private IEnumerator SpawnContiniuously()
    {
        yield return new WaitForSeconds(delay);
        if (!isFixed) SpawnObject();
    }
}
