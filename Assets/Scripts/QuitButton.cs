﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class QuitButton : MonoBehaviour
{
    private Interactable interactableObject;
    private Vector3 pos;
    private Quaternion rot;

    void Start()
    {
        interactableObject = GetComponent<Interactable>();
        pos = transform.position;
        rot = transform.rotation;
    }
    
    void FixedUpdate()
    {
        if (interactableObject.attachedToHand != null)
        {
            Application.Quit();
        }
        transform.position = pos;
        transform.rotation = rot;
    }
}
