﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelVolume : MonoBehaviour
{
    static float volume;
    static LevelVolume Instance;
    static float detailVolume;

    private void Start()
    {
        if (Instance == null) Instance = this; else
        {
            Destroy(this);
            return;
        }
        foreach (var i in GetComponentsInChildren<MeshFilter>())
        {
            volume += MeshMath.Calculate(i.sharedMesh);
        }
    }
}
