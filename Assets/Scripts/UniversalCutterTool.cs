﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MeshSplitting.Splitables;

public class UniversalCutterTool : MonoBehaviour
{
    public Transform plane;
    public float cutFrequencyTreshold;

    private float nextCutTime;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<Splitable>() != null && Time.time >= nextCutTime)
        {
            collision.gameObject.GetComponent<Splitable>().Split(plane);
            nextCutTime = Time.time + 1 / cutFrequencyTreshold;
        }
    }
}
