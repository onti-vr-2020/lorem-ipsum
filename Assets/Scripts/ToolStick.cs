﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ToolStick : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
        Tool tool = collision.gameObject.GetComponent<Tool>();
        if (tool != null)
        {
            if (Time.time - tool.lastInHand < 0.06f)
            {
                Debug.Log("Attach");
                collision.gameObject.GetComponent<Interactable>().attachedToHand.DetachObject(collision.gameObject);
                rb.isKinematic = true;
                collision.transform.parent = transform;
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
        Interactable interactable = collision.gameObject.GetComponent<Interactable>();

        if (interactable != null)
        {
            if (collision.gameObject.CompareTag("Tool") && interactable.attachedToHand != null)
            {
                interactable.attachedToHand.DetachObject(collision.gameObject);
                rb.isKinematic = false;
                collision.transform.parent = null;
            }
        }
    }
}