﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ReloadScene : MonoBehaviour
{
    private Interactable interactableObject;
    private Vector3 pos;
    private Quaternion rot;
    void Start()
    {
        interactableObject = GetComponent<Interactable>();
        pos = transform.position;
        rot = transform.rotation;
    }

    void FixedUpdate()
    {
        if (interactableObject.attachedToHand != null)
        {
            foreach (var i in FindObjectsOfType<Transform>())
            {
                Destroy(i.gameObject);
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
        transform.position = pos;
        transform.rotation = rot;
    }
}
