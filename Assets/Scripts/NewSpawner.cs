﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class NewSpawner : MonoBehaviour
{
    public GameObject spawnObject;
    public GameObject prefab;

    private GameObject copy;

    void Start()
    {
        copy = Instantiate(spawnObject, transform.position, Quaternion.identity);
        copy.GetComponent<CopiedObject>().prefab = prefab;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == copy)
        {
            other.gameObject.GetComponent<CopiedObject>().toEnlarge = true;
            copy = Instantiate(spawnObject, transform.position, Quaternion.identity);
            copy.GetComponent<CopiedObject>().prefab = prefab;
            copy.GetComponent<Interactable>().enabled = false;
            copy.GetComponent<Throwable>().enabled = false;

            Rigidbody copyRigidbody = copy.GetComponent<Rigidbody>();
            copyRigidbody.isKinematic = true;

            StartCoroutine(Spawn(copy));
        }
    }

    private IEnumerator Spawn(GameObject go)
    {
        float startTime = Time.time;
        float spawnDuration = 0.5f;
        for (float scale = 0; !Mathf.Approximately(scale, 0.1f); scale = Mathf.Lerp(0, 0.1f, (Time.time - startTime) / spawnDuration))
        {
            go.transform.localScale = new Vector3(scale, scale, scale);
            yield return null;
        }
        go.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        go.GetComponent<Interactable>().enabled = false;
        go.GetComponent<Throwable>().enabled = false;

        Rigidbody copyRigidbody = go.GetComponent<Rigidbody>();
        copyRigidbody.isKinematic = false;
    }
}