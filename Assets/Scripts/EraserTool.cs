﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;
using Valve.VR.Extras;

public class EraserTool : MonoBehaviour
{
    private Interactable inter;
    [SerializeField] private SteamVR_LaserPointer laser;
    private SteamVR_Behaviour_Pose pose;

    // Start is called before the first frame update
    void Start()
    {
        inter = GetComponent<Interactable>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (laser.transform.childCount == 0) return;
        if (inter.attachedToHand != null)
        {
            inter.attachedToHand.ResetAttachedTransform(inter.attachedToHand.AttachedObjects[0]);
            pose = inter.attachedToHand.transform.GetComponent<SteamVR_Behaviour_Pose>();
            laser.pose = pose;
            //laser.transform.GetChild(0).gameObject.SetActive(true);
            laser.gameObject.SetActive(true);
            if (laser.interactWithUI.GetState(pose.inputSource))
            {
                RaycastHit hit;
                if (Physics.Raycast(laser.transform.position, laser.transform.forward, out hit, 100))
                {
                    var e = hit.transform.gameObject;
                    if (e.CompareTag("Wood") || e.CompareTag("Metal") || e.CompareTag("Rock"))
                        Destroy(e.gameObject);
                }

            }
        }
        else
        {
            //laser.transform.GetChild(0).gameObject.SetActive(false);
            laser.gameObject.SetActive(false);
        }
        
    }

}
