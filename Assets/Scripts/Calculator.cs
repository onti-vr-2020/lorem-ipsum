﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculator : MonoBehaviour
{
    public static Calculator Instance;


    private void Start()
    {
        Instance = this;
    }

    public float Calculate(Vector3 max, Vector3 min, float delta)
    {
        float volume = 0;
        var size = new Vector3(delta / 2, delta / 2, delta / 2);
        Debug.DrawLine(min, max, Color.red, 20);
        for (float x = min.x; x < max.x; x += delta)
        {
            //yield return null;
            for (float y = min.y; y < max.y; y += delta)
            {
                for (float z = min.z; z < max.z; z += delta)
                {
                    RaycastHit hit;
                    Debug.Log("s");
                    if (Physics.BoxCast(new Vector3(x, y, z), size, Vector3.forward, out hit, Quaternion.identity, delta * 2, 11, QueryTriggerInteraction.Collide))
                    {
                        Debug.Log(hit.collider.name);
                        if (Physics.BoxCast(new Vector3(x, y, z), size, Vector3.forward, out hit, Quaternion.identity, delta * 2, 12))
                        {
                            Debug.Log("okok");
                            volume += 0.000001f;
                        }
                    }
                }
            }
        }
        return volume;
    }
}
