﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CopiedObject : MonoBehaviour
{
    [HideInInspector]
    public GameObject prefab;
    [HideInInspector]
    public bool toEnlarge;

    private Interactable interactableObject;
    private bool isEnlarged = false;


    void Start()
    {
        interactableObject = GetComponent<Interactable>();
        GetComponent<MeshRenderer>().material = prefab.GetComponent<MeshRenderer>().sharedMaterial;
    }
    
    void FixedUpdate()
    {
        if (interactableObject.attachedToHand == null && !isEnlarged && toEnlarge)
        {
            isEnlarged = true;
            StartCoroutine(Enlarge());
        }
    }

    private IEnumerator Enlarge()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        var go = Instantiate(prefab, transform.position, Quaternion.identity);
        transform.parent = go.transform;
        float startTime = Time.time;
        float spawnDuration = 1f;

        for (float scale = 0; !Mathf.Approximately(scale, 1); scale = Mathf.Lerp(0, 1, (Time.time - startTime) / spawnDuration))
        {
            go.transform.localScale = new Vector3(scale, scale, scale);
            transform.localScale = transform.localScale * 0.8f;
            yield return null;
        }
        go.transform.localScale = new Vector3(1, 1, 1);

        Destroy(gameObject);
    }
}