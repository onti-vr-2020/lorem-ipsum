﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Detail : MonoBehaviour
{
    public LayerMask mask;
    public bool isAttached;
    public List<Detail> children;
    public float volume;

    private Rigidbody rb;
    private Interactable interactable;
    private bool isInHand;
    private bool attachPending;
    private float lastInHand;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        interactable = GetComponent<Interactable>();
        children = new List<Detail>();
    }

    private void FixedUpdate()
    {
        isInHand = interactable.attachedToHand != null;
        if (isInHand) lastInHand = Time.time;
        if (!isInHand && attachPending)
        {
            isAttached = true;
            transform.position = new Vector3((int)(transform.position.x * 100) / 100f, (int)(transform.position.y * 100) / 100f, (int)(transform.position.z * 100) / 100f);
            rb.isKinematic = true;
            attachPending = false;
            gameObject.layer = 12;
            StartCoroutine(CalculateVolume());
        }
        if (isInHand && isAttached)
        {
            Detach();
        }
        if (!isAttached && !isInHand && rb.isKinematic)
        {
            rb.isKinematic = false;
        }
    }

    public void Detach()
    {
        volume = 0;
        isAttached = false;
        attachPending = false;
        gameObject.layer = 0;
        foreach (var i in children)
        {
            i.Detach();
        }
        children.Clear();
    }

    private IEnumerator CalculateVolume()
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        var c = GetComponent<Collider>();
        var min = c.bounds.min;
        var max = c.bounds.max;
        float delta = 0.25f;
       // volume = Calculator.Instance.Calculate(max, min, delta);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var other = collision.gameObject.GetComponent<Detail>();
        bool attached = false;
        if (other != null) attached = other.isAttached;
        if (Time.time - lastInHand < 0.06f && (collision.gameObject.CompareTag("CraftingTable") || attached))
        {
            if (!collision.gameObject.CompareTag("CraftingTable")) other.children.Add(this);
            attachPending = true;
        }
    }
}
