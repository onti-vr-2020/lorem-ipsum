﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XZFollower : MonoBehaviour
{
    public Transform parent;
    public float offset = -0.7f;
    private float lastY;

    private void Start()
    {
        lastY = transform.rotation.y;
    }

    void Update()
    {
        transform.position = new Vector3(parent.position.x, parent.position.y + offset, parent.position.z);
        transform.Rotate(0, lastY - parent.rotation.y , 0);
        lastY = transform.rotation.y;
    }
}
