﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Tool : MonoBehaviour
{
    private Interactable inter;
    public float lastInHand;

    void Start()
    {
        inter = GetComponent<Interactable>();
    }

    private void FixedUpdate()
    {
        if (inter.attachedToHand != null) lastInHand = Time.time;
    }
}
