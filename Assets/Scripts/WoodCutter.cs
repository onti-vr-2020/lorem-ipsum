﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MeshSplitting.Splitables;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class WoodCutter : MonoBehaviour
{
    public Transform plane;
    public float cutFrequencyTreshold;
    public Transform cutNormal;

    private Splitable toCut;
    private float progress;
    public float velocityTreshold;
    public float powerMultiplier;
    private Interactable inter;
    private Rigidbody rb;
    private Hand hand;
    private SteamVR_Behaviour_Pose handPose;
    public SteamVR_Action_Boolean pose;
    private Vector3 lastPosition;
    public Collider cd;

    private void Start()
    { 
        inter = GetComponent<Interactable>();
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        //Debug.Log(transform.right);
        if (hand != null && toCut != null)
        {
            if (!pose.GetState(handPose.inputSource))
            {
                rb.isKinematic = false;
                toCut = null;
                cd.enabled = true;
            } else
            {
                float sign = 0;
                var delta = hand.transform.position - lastPosition;
                if (Mathf.Sign(transform.right.x) == Mathf.Sign(delta.normalized.x)) sign++; else sign--;
                if (Mathf.Sign(transform.right.y) == Mathf.Sign(delta.normalized.y)) sign++; else sign--;
                if (Mathf.Sign(transform.right.z) == Mathf.Sign(delta.normalized.z)) sign++; else sign--;
                delta = transform.right * delta.magnitude * Mathf.Sign(sign);
                //delta.Scale);
                transform.Translate(delta, Space.World);
                transform.Translate(-transform.up * (delta.magnitude * powerMultiplier*0.2f), Space.World);
                progress += delta.magnitude * powerMultiplier;
                lastPosition = hand.transform.position;
                
                if (progress >= 1 && toCut != null)
                {
                    StartCoroutine(Split(toCut));
                    toCut = null;
                }
            }
        }
    }

    private IEnumerator Split(Splitable toCut)
    {
        transform.SetParent(null);
        rb.isKinematic = false;
        cd.enabled = true;
        yield return new WaitForFixedUpdate();
        //yield return new WaitForFixedUpdate();
        toCut.Split(plane);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<Splitable>() != null && collision.CompareTag("Wood") && inter.attachedToHand != null)
        {
            toCut = collision.gameObject.GetComponent<Splitable>();
            hand = inter.attachedToHand;
            handPose = hand.transform.GetComponent<SteamVR_Behaviour_Pose>();
            inter.attachedToHand.DetachObject(gameObject);
            transform.SetParent(collision.transform);
            lastPosition = hand.transform.position;
            cd.enabled = false;
            rb.isKinematic = true;
            progress = 0;
        }
    }
    /*
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Splitable>() == toCut && toCut != null)
        {
            rb.isKinematic = false;
            toCut = null;
            cd.enabled = true;
        } 
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (progress >= 1 && toCut != null)
        {
            toCut.Split(plane);
        }
    }
*/
}
